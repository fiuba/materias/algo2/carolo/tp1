#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#ifndef L_SIZE
    #define L_SIZE 256
#endif // L_SIZE
#ifndef C_SIZE
    #define C_SIZE 64
#endif // C_SIZE

//#define DEBUG

typedef struct {
	 FILE* f;
	 char delimitador;
	 char escape;
	 char linea[L_SIZE];
	 char* campos[C_SIZE];
	 int num_campos;
	 int leido;
	 int creado;
	 int feof;
} TParser;

int PARS_Crear (TParser * parser, char archivo[], char delimitador, char escape);
int PARS_ObtLinea (TParser * parser);
int PARS_ObtCampo (TParser * parser, int n, char campo[]);
int PARS_ObtQCampos (TParser * parser);
void PARS_Destruir (TParser * parser);
void limpiar_escapes(TParser* parser);
int test_0();
int mostrar_menu();
int _getch() { return fflush(stdin), getch(); }
int _getche() { return  fflush(stdin), getche(); }
//!=================================================================================

int main(int argc, char **argv)
{
	//return test_0();

	TParser p;
    TParser* parser = &p;
    parser->creado = 0;
	char opcion, cont;

	puts("El programa admite archivos con lineas de hasta 255 caracteres,\n con un maximo de 64 campos por linea.\n\n---\n\n");


    while ( (opcion = (mostrar_menu(), _getch())) != 'x' && opcion != 'X')
    {
        switch (opcion)
        {
            case '1': //Abrir archivo para �parsear�
            {
                char archivo[99];
                FILE* ff;
                char delimitador, escape;
                char res;
                int x = 0;

                do
                {
                    res = 'n';
                    system("cls");
                    puts("Ingrese la direccion del archivo: \n");
                    gets(archivo);
                    if ((ff = fopen(archivo, "r")) == NULL)
                    {
                        puts("ERROR. No se puede acceder al arhivo.\n Desea reintentarlo? (s/n)");
                        res = _getch();
                        x = !(res == 's' || res == 'S');
                    }
                    else
                        fclose(ff);
                } while(res == 's' || res == 'S');
                if (x) continue;

                do
                {
                    res = 'n';
                    puts("Ingrese el caracter delimitador: ");
                    delimitador = _getche(); puts("\n");
                    if (delimitador == 13)
                    {
                        puts("ERROR. Caracter delimitador invalido.\n Desea reintentarlo? (s/n)");
                        res = _getch();
                        x = !(res == 's' || res == 'S');
                    }
                } while(res == 's' || res == 'S');
                if (x) continue;

                do
                {
                    res = 'n';

                    puts("Ingrese el caracter de escape: ");
                    escape = _getche(); puts("\n");
                    if (escape == delimitador)
                    {
                        puts("ERROR. Caracter de escape invalido.\n Desea reintentarlo? (s/n)");
                        res = _getch();
                        x = !(res == 's' || res == 'S');
                    }
                } while(res == 's' || res == 'S');
                if (x) continue;

                if (parser->creado)
                    PARS_Destruir(parser);


                system("cls");
                if(PARS_Crear(parser, archivo, delimitador, escape))
                    puts("ERROR al abrir el archivo.\n\n");
                else
                    puts("Archivo abierto correctamente.\n\n");

                system("pause");

            }
            break;
            case '2': //Obtener nueva l�nea del archivo}
                {
                    system("cls");

                    switch(PARS_ObtLinea(parser))
                    {
                    case 0:
                        puts("Linea leida correctamente.\n\n");
                        break;
                    case 1:
                        puts("ERROR. NO hay mas lineas para leer.\n\n");
                        break;
                    case 2:
                        puts("ERROR. Primero debe abrir un archvo.\n\n");
                        break;
                    default :
                        puts("ERROR desconocido.");
                        break;
                    }
                    system("pause");
                }
            break;
            case '3': //Obtener la cantidad de campos que contiene la ultima l�nea obtenida
                {
                    system("cls");

                    if (!parser->creado)
                        puts("ERROR. Primero debe abrir un archvo.\n\n");
                    else if (!parser->leido)
                        puts("ERROR. Primero debe leer una linea.\n\n");
                    else
                        printf("Se leyeron %d campos en la ultima linea leida.\n\n", PARS_ObtQCampos(parser));


                    system("pause");
                }
            break;
            case '4': //Obtener un campo de la ultima l�nea obtenida
                {
                    system("cls");

                    if (!parser->creado)
                        puts("ERROR. Primero debe abrir un archvo.\n\n");
                    else if (!parser->leido)
                        puts("ERROR. Primero debe leer una linea.\n\n");
                    else if (PARS_ObtQCampos(parser)<1)
                        puts("Primero debe leer una linea.\n\n");
                    else
                    {
                        char campo[99];
                        char num[10];
                        char res;
                        int n, x = 0;
                        do
                        {
                            res = 'n';

                            printf("Ingrese el numero del campo. [0-%d] ", PARS_ObtQCampos(parser)-1);
                            gets(num);
                            n = atoi(num);

                            if (n < 0)
                            {
                                puts("ERROR. No se reconocio un numero valido.\n Desea reintentarlo? (s/n)");
                                res = _getch();
                                x = !(res == 's' || res == 'S');
                            }
                            else if(n >= PARS_ObtQCampos(parser))
                            {
                                puts("ERROR. El numero ingresado excede la cantidad de campos de la linea.\n Desea reintentarlo? (s/n)");
                                res = _getch();
                                x = !(res == 's' || res == 'S');
                            }
                            else if (PARS_ObtCampo(parser, n, campo))
                            {
                                puts("ERROR al obtener el campo.\n\n");
                                system("pause");
                                x = 1;
                            }
                        } while(res == 's' || res == 'S');
                        if (x) continue;

                        printf("campo[%d]: %s \n\n", n, campo);
                    }

                    system("pause");
                }
            break;
            case '5': //Cerrar archivo
                {
                    if (parser->creado)
                        PARS_Destruir(parser);

                    system("cls");
                    puts("Archivo cerrado.\n\n");
                    system("pause");
                }
            break;
            default:
            {
                system("cls");
                puts("Opcion no recononocida.\n\nDebe ingresar un numero: [1-5] o 'x' para salir\n");
                puts("----------------------------------\n");
                system("pause");
            }
        }
    }

    if (parser->creado)
        PARS_Destruir(parser);

    system("cls");
    puts("\nFin del programa.\n\n");
    system("pause");

	return 0;
}

int PARS_Crear (TParser * parser, char archivo[], char delimitador, char escape)
{
/* Crea el parser para procesar el archivocuyo nombre fue pasado por par�metro,
 * para el cual se utilizar�n como car�cter delimitador y de escape los indicados.
 *
 * PRE:		�parser� no creado,
 * 			�delimitador� es un caracter no nulo,
 * 			�escape� es un caracter no nulo y distinto de �delimitador�.
 * POST:	devuelve 0 si no hubieron errores,  sino
 * 			devuelve 1 si el parser no ha sido creado
 *          devuelve 2 si el caracter delimitador es nulo
 *          devuelve 3 si el caracter de escape es igual al delimitador
 */

    int err = 0;

         if (parser->creado)        err = 1;
    else if (!delimitador)          err = 2;
    else if (delimitador == escape) err = 3;

    if (!err)
    {
		parser->delimitador = delimitador;
        parser->escape = escape;

        if ((parser->f = fopen(archivo, "r")) == NULL)
            err = 4;
        else if (feof(parser->f))
            err = 5;
        else
        {
            parser->creado = 1;
            parser->leido = 0;
            parser->feof = feof(parser->f);
            parser->campos[0] = 0;
        }
	}

	return err;
}

int PARS_ObtLinea (TParser * parser )
{
/* Obtiene la pr�xima l�nea del archivo y la retiene para su procesamiento.
 *
 * PRE:		�parser� creado.
 * POST:	devuelve 0 si pudo obtener una l�nea (sin errores)
 * 			devuelve 1 si no existen mas l�neas para leer en el archivo
 *          devuelve 2 si el parser no ha sido creado
 */

    int err = 0;
    char* linea = parser->linea;

	if (! parser->creado)
		err = 2;
	else
    {
        parser->leido = 0;

        if (parser->feof)
        {
            err = 1;
        }
        else
        {
            linea = fgets(linea, L_SIZE, parser->f);

            if ( linea == NULL )
                err = 1; //no existen mas l�neas para leer en el archivo
            else
            {
               //linea = strtok(linea, "\n");

                parser->campos[0] = linea;
                int c = 1;
                int i = 0;
                int llen= strlen(linea) - 1;
                //! strlen(linea) -1 porque el ultimo caracter no puede ser un delimitador,
                // porque si lo fuera, indicaria que en "el siguiente" inicia el "proximo campo"

                #ifdef DEBUG
                puts("--- PARS_ObtLinea -----------\n");

                printf("linea:\n%s\n",linea);
                puts("---\n");
                printf("delim: %c\n", parser->delimitador);
                puts("---\n");

                #endif // DEBUG

                while( i < llen)
                {
                    if (linea[i] == parser->delimitador)
                    {
                        if (!i || linea[i-1] != parser->escape)
                        {
                            #ifdef DEBUG
                            printf("linea[%d] = %c\n", i, linea[i]);
                            #endif // DEBUG
                            linea[i] = 0;
                            parser->campos[c++] = linea + i + 1; //! ( i < strlen(linea) -1 )

                        }
                    }
                    i++;
                }
                if( linea[i] == '\n' ) linea[i] = 0; // borrar salto de linea del ultimo campo

                parser->num_campos = c;
                limpiar_escapes(parser);
            }
        }
    }

    #ifdef DEBUG
    puts(linea);
    puts("-----------------------------\n");
    #endif // DEBUG

    parser->leido = !err;
    parser->feof = feof(parser->f);
	return err;
}

int PARS_ObtCampo (TParser * parser, int n, char campo[])
{
/* Obtiene el n-�simo campo de la ultima l�nea obtenida del archivo.
 *
 * PRE:		parser creado, l�nea obtenida.
 * POST:	Si pudo obtener el campo N de la l�nea
 * 				devuelve 0 (sin errores) y el campo obtenido,
 * 			sino devuelve 1.
 */

    if ( !parser->creado || !parser->leido || n >= PARS_ObtQCampos(parser) )
		return 1;
	else
        strcpy(campo, parser->campos[n]);
    return 0;
}

int PARS_ObtQCampos (TParser * parser)
{
/* Devuelve la cantidad de campos de la ultima l�nea obtenida del archivo.
 *
 * PRE:		parser creado, l�nea obtenida
 * POST:	Devuelve la cantidad de campos de la ultima l�nea obtenida del archivo.
 */

    if ( !parser->creado || ! parser->leido)
		return -1;

    return parser->num_campos;
}

void PARS_Destruir (TParser * parser)
{
/* Destruye el parser.
 *
 * PRE:		parser creado.
 * POST:	parser destruido.
 */

    if (parser->creado)
    {
        fclose(parser->f);
        parser->creado = 0;
        parser->leido = 0;
    }

 }

void limpiar_escapes(TParser* parser)
{
    char* c;
    char e = parser->escape;
    int r;

    for (int i = 0; i < parser->num_campos; i++)
    {
        c = parser->campos[i];
        r = 0;
        for(int j = 0; j < strlen(c); j++)
        {
            if (c[j] == e) r++;
            if (r>0) c[j] = c[j+r];
        }
    }

}

int test_0()
{
    TParser p;
    TParser* parser = &p;
    parser->creado = 0;

    PARS_Crear(parser, "test\\reg.txt", ',', '%');

    if (!parser->creado)
        puts("ERROR. No se pudo crear el parser.");
    else
    {
        while (!PARS_ObtLinea(parser))
        {
            char campo[99];
            int num_campos;


            num_campos = PARS_ObtQCampos(parser);

            #ifdef DEBUG
            puts("--- main --------------------\n");
            printf("linea:\t %s\n", parser->linea);
            printf("num_campos:\t %d\n", num_campos);
            printf("campo:\t %s\n", parser->campos[1]);
            puts("-----------------------------\n");
            #endif // DEBUG

            for (int i = 0; i < num_campos; i++)
            {
                if( PARS_ObtCampo(parser, i, campo) )
                    puts("ERROR. No se pudo obtener el campo");
                else
                    printf("i: %d \n   campo: %s \n   parser->campos[i]: %s \n", i, campo, parser->campos[i]);
            }

            puts("------------------------------");
        }
    }

    return 0;
}

int mostrar_menu()
{
    system("cls");

    puts("Seleccione una opcion:\n");
	puts("----------------------------------\n");
	puts("1. Abrir archivo.");
	puts("2. Obtener nueva linea.");
	puts("3. Obtener la cantidad de campos de la linea leida.");
	puts("4. Obtener un campo de la linea leida.");
	puts("5. Cerrar archivo.");
	puts("x. Salir");
	puts("\n----------------------------------\n");
	return 0;
}
