# Trabajo Practico 1
## Abstraccion

------------

#### Algoritmos y Programación II [75.41]
#### Cátedra Lic. Gustavo Carolo
#### 1º Cuatrimestre 2017

------------

### Introducción: "Parsing" de un texto
Muchas veces en distintas aplicaciones que debemos construir existe la necesidad de leer un archivo de texto plano y descomponer sus líneas en campos.
Las líneas de estos archivos de texto tienen sus campos separados por un carácter llamado “delimitador”.  El carácter delimitador indica la separación entre dos campos de la línea.
A veces puede ocurrir que uno de los campos del archivo contenga al carácter delimitador, en estos casos se utiliza un carácter de “escape” para indicar que este caracter no debe considerarse como delimitador.
Por ejemplo: si tenemos un archivo separado por comas “,” y uno de los campos del archivo es un campo de texto que puede contener comas, para salvar esta solución podemos indicar que se va a utilizar el carácter “\” como carácter de escape, de tal manera que si se encuentra la combinación “\,” dentro de la línea a descomponer podemos determinar que esa coma no esta ahí como delimitador sino como parte integrante de un campo de texto.
Un ejemplo claro de este tipo de archivos son los archivos “.csv”  cuyos valores están separados por comas.

#### Ejemplo
Dado el siguiente archivo “clientes.csv”, delimitado por “,” con carácter de escape “\”, cuyos campos son Nº Cliente, Apellido, Nombre y DNI:
`
1,SALVAY,ESTEBAN,24960413
2,GIGENA,RUBEN HUMBERTO,11187486
3,DUARTE GONCALVES,MARILIA,93351728
5,REDOLFI,EDUARDO OSCAR,4749123
6,MERIN,MIRTA NILDA,12279227
7,ROJAS,GERVASIO,11107755
9,URRICHE,JORGE L\,,4749122
10,FERREIRA DE FORTE,,4749121
`

Podemos “parsear” el mismo en los siguientes campos:
`
Fila 1:	Nº=1	Apellido=SALVAY	Nombre=ESTEBAN	DNI=24960413
Fila 2:	Nº=2 	Apellido=GIGENA 	Nombre=RUBEN HUMBERTO 	DNI=11187486
Fila 3:	Nº=3 	Apellido=DUARTE GONCALVES 	Nombre=MARILIA 	DNI=93351728
Fila 4:	Nº=5 	Apellido=REDOLFI 	Nombre=EDUARDO OSCAR 	DNI=4749123
Fila 5:	Nº=6 	Apellido=MERIN 	Nombre=MIRTA NILDA 	DNI=12279227
Fila 6:	Nº=7 	Apellido=ROJAS 	Nombre=GERVASIO 	DNI=11107755
Fila 7:	Nº=9 	Apellido=URRICHE 	Nombre=JORGE L, 	DNI=4749122
Fila 8:	Nº=10	Apellido=FERREIRA DE FORTE Nombre=	DNI=4749121
`

## Enunciado del TP
El propósito de este trabajo práctico es construir un TDA que encapsule esta funcionalidad de “parseo” de archivos de texto para poder realizar estas operaciones de forma transparente desde cualquier aplicación.

**1. ** Implementar un TDA “TParser” que, dado un archivo, un caracter delimitador y un carácter de escape, permita la captura de las líneas de este archivo para ir obteniendo de ellas los campos delimitados por el carácter indicado.

#### Diseño del tipo de dato del TDA
La implementación del tipo de dato del TDA queda a criterio del grupo.

#### Primitivas del TDA
Deben desarrollarse las siguientes primitivas para el TDA, respetando exactamente la interfaz y la funcionalidad indicadas.

`int PARS_Crear (TParser * parser, char[] archivo, char delimitador, char escape);`

| Función | Crea el parser para procesar el archivo cuyo nombre fue pasado por parámetro, para el cual se utilizarán como carácter delimitador y de escape los indicados. |
| --- | --- |
| PRE | “parser” no creado, “delimitador” es un caracter no nulo, “escape” es un caracter no nulo y distinto de “delimitador”. |
| POST | Si no hubo error “parser” creado y devuelve 0, sino devuelve 1. |

`int PARS_ObtLinea (TParser * parser );`

| Función | Obtiene la próxima línea del archivo y la retiene para su procesamiento. |
| --- | ---- |
| PRE | “parser” creado. |
| POST | Si pudo obtener una línea devuelve 0 (sin errores), sino devuelve 1 (no existen mas líneas para leer en el archivo). |

`int PARS_ObtCampo (TParser * parser, int N, char[] campo );`

| Función | Obtiene el n-ésimo campo de la ultima línea obtenida del archivo. |
| --- | --- |
| PRE | parser creado, línea obtenida |
| POST | Si pudo obtener el campo N de la línea devuelve 0 (sin errores) y el campo obtenido, sino devuelve 1. |

`int PARS_ObtQCampos (TParser * parser);`

| Función | Devuelve la cantidad de campos de la ultima línea obtenida del archivo. |
| --- | --- |
| PRE | parser creado, línea obtenida  |
| POST | Devuelve la cantidad de campos de la ultima línea obtenida del archivo. |

`void PARS_Destruir (TParser * parser);`

| Función | Destruye el parser. |
| --- | --- |
| PRE | parser creado. |
| POST | parser destruido. |

**2. ** Implementar un programa que, utilizando el TDA T_PARSER antes definido, proponga las siguientes opciones por pantalla al usuario:
1. Abrir archivo para “parsear”: En esta opción el usuario deberá indicar el nombre del archivo que desea parsear y el carácter delimitador que debe utilizarse.
2. Obtener nueva línea del archivo.
3. Obtener la cantidad de campos que contiene la ultima línea obtenida.
4. Obtener un campo de la ultima línea obtenida: En esta opción el usuario deberá indicar el numero del campo que desea obtener.
5. Cerrar archivo.
6. Salir del programa.

